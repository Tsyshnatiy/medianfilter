TEMPLATE = app
CONFIG += console
CONFIG -= qt

SOURCES += \
    MedianFilter1D.cpp \
    RingBuffer.cpp \
    main.cpp

HEADERS += \
    MedianFilter1D.h \
    RingBuffer.h

