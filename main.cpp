// ---------------------------------------------------------------------
// Author: Vladimir Tsyshnatiy 24.08.2014
// ---------------------------------------------------------------------

#include <stdio.h>
#include <vector>
#include "MedianFilter1D.h"
#include "MedianFilter1D.cpp"

#define FILTER_WINDOW 3
#define DATA_LEN 10

template <typename T, size_t N>
T* begin(T(&arr)[N]) { return &arr[0]; }
template <typename T, size_t N>
T* end(T(&arr)[N]) { return &arr[0] + N; }

template <typename T>
void compare(vector<T> *input, vector<T> *output) {
    std::cout << "input data:  ";
    for (typename std::vector<T>::const_iterator it = input->begin(); it != input->end(); ++it) {
        std::cout << *it << " ";
    }
    std::cout << std::endl << "output data: ";
    for (typename std::vector<T>::const_iterator it = output->begin(); it != output->end(); ++it) {
        std::cout << *it << " ";
    }
    std::cout << std::endl;
}

int main(int argc, char *argv[]) {
    float data[DATA_LEN] = {1.0f,20.0f,3.0f,6.0f,2.0f,8.0f,19.0f,57.0f,6.0f,34.0f};
    vector<float> *input = new vector<float>(begin(data), end(data));
    MedianFilter1d<float> mf(3);
    vector<float> *output = mf.apply(input);
    compare(input, output);
    return 0;
}

