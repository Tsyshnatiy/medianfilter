#ifndef RINGBUFFER_H
#define RINGBUFFER_H

#include <iostream>
#include <vector>

// ---------------------------------------------------------------------
// Author: Vladimir Tsyshnatiy 24.08.2014
// ---------------------------------------------------------------------

template<class T>
class RingBuffer
{
private:
    std::vector<T> buffer;
    int start_pos;
public:
    RingBuffer(const int size, const T defVal);
    RingBuffer(const RingBuffer& other); //copy constructor
    RingBuffer(const T* initCollection, const int colSize, const int bufSize);
    explicit RingBuffer(const int size);

    virtual ~RingBuffer() {}

public:
    int getSize() const { return buffer.size()/2; }
    void resize(size_t size);
    std::vector<T> getBuffer() const { return buffer; }
    void push(const T item); //copies item to container
    std::vector<T> sort() const; //sorts in ascending order
    T median() const;
    void print(std::ostream& strm);
    void fill(T value);
};

#endif // RINGBUFFER_H
