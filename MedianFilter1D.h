// ---------------------------------------------------------------------
// Author: Vladimir Tsyshnatiy 24.08.2014
// ---------------------------------------------------------------------

#ifndef MEDIANFILTER1D_H
#define MEDIANFILTER1D_H
#include "RingBuffer.h"

#include <vector>

template<typename T>
class MedianFilter1d
{
private:
    RingBuffer<T> rb;
public:
    explicit MedianFilter1d(const int filterSize);
    std::vector<T> *apply(const std::vector<T> *signal);
    virtual ~MedianFilter1d() {}
};

#endif // MEDIANFILTER1D_H
