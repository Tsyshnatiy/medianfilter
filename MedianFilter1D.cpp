// ---------------------------------------------------------------------
// Author: Vladimir Tsyshnatiy 24.08.2014
// ---------------------------------------------------------------------

#include <stdio.h>
#include <algorithm>
#include "MedianFilter1D.h"
#include "RingBuffer.cpp"
#include <stdexcept>
#include <vector>

using namespace std;

template<typename T>
MedianFilter1d<T>::MedianFilter1d(const int filterSize):
    rb(filterSize)
{
    if (filterSize % 2 == 0 || filterSize < 0) {
        throw invalid_argument("Filter size is invalid");
    }
}

template<typename T>
std::vector<T>* MedianFilter1d<T>::apply(const std::vector<T> *input)
{
    if (input->size() < rb.getSize()) {
        throw invalid_argument("Signal length is invalid");
    }
    vector<T> *output = new vector<T>();
    // filter step
    for (typename std::vector<T>::const_iterator it = input->begin(); it != input->end(); ++it) {
        rb.push(*it);
        output->push_back(rb.median());
        rb.print(cout);
    }

    for (int j = 0 ; j < rb.getSize()/ 2 ; ++j) {
        rb.push(0);
        output->push_back(rb.median());
        rb.print(cout);
    }
    return output;
}

