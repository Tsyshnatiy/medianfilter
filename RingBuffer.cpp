// ---------------------------------------------------------------------
// Author: Vladimir Tsyshnatiy 24.08.2014
// ---------------------------------------------------------------------

#include "RingBuffer.h"

#include <algorithm>

template<typename T>
RingBuffer<T>::RingBuffer(const int size, const T defVal):
    buffer(0)
{
    this->start_pos = 0;
    this->resize(size);
    std::fill(buffer.begin(), buffer.end(), defVal);
}

template<typename T>
RingBuffer<T>::RingBuffer(const int size)
{
    this->resize(size);
}

template<typename T>
RingBuffer<T>::RingBuffer(const T* initCollection, const int colSize, const int size)
{
    this->start_pos = 0;
    this->resize(2*size);
    for (int i = 0; i < size && i < colSize; ++i) {
        push(initCollection[i]);
    }
}

template<typename T>
RingBuffer<T>::RingBuffer(const RingBuffer& other)
{
    this->resize(other.getSize());
    std::copy(other.buffer.begin(), other.buffer.end(), this->buffer.begin());
}

template<typename T>
void RingBuffer<T>::resize(size_t newSize) {
    if (this->start_pos > newSize) start_pos = 0;
    buffer.resize(2*newSize-1);
}

template<typename T>
void RingBuffer<T>::push(const T item)
{
    if (--start_pos < 0) {
        start_pos = buffer.size()/2;
        buffer[start_pos] = item;
    } else {
        buffer[start_pos] = buffer[start_pos+buffer.size()/2+1] = item;
    }
}

template<typename T>
void RingBuffer<T>::fill(T value)
{
    std::fill(buffer.begin(), buffer.end(), value);
}

template<typename T>
std::vector<T> RingBuffer<T>::sort() const
{
    std::vector<T> temp;
    std::copy(buffer.begin() + start_pos,
              buffer.begin() + buffer.size()/2 + start_pos - 1,
              std::back_inserter(temp));
    std::sort(temp.begin(), temp.end());
    return temp;
}

template<typename T>
T RingBuffer<T>::median() const
{
    std::vector<T> temp;
    std::copy(buffer.begin() + start_pos,
              buffer.begin() + buffer.size()/2 + start_pos + 1,
              std::back_inserter(temp));
    std::sort(temp.begin(), temp.end());
    return temp[(temp.size()-1)/2];
}

template<typename T>
void RingBuffer<T>::print(std::ostream& strm)
{
    strm << "Ring buffer: ";
    int i = 0;
    for (typename std::vector<T>::iterator it = buffer.begin(); it != buffer.end(); it++, i++) {
        if (i == start_pos) {
            strm << "[" << *it << "\t";
        } else
        if (i == start_pos + buffer.size()/2) {
            strm << *it << "]" << "\t";
        } else {
            strm << *it << "\t";
        }
    }
    strm << std::endl;
}
